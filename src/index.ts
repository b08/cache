import { Cache } from "./cache";
export * from "./cache";
export * from "./cacheProvider";
export * from "./cacheKey.type";
export * from "./cacheSettings.type";
export * from "./memoize";

export const cache = new Cache();
