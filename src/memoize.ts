import { nullObject } from "@b08/object-map";
import { Cache } from "./cache";
import { CacheSettings } from "./cacheSettings.type";

export interface MemoizedFunc<TP extends any[], TR> {
  (...p: TP): TR;
  reset: () => void;
}

let defaultMemoizeSettings: CacheSettings;
export function setDefaultMemoizeSettings(settings?: CacheSettings): void {
  defaultMemoizeSettings = settings;
}

export const memoize = function memoize<TP extends any[], TR>(func: (...p: TP) => TR, settings?: CacheSettings): MemoizedFunc<TP, TR> {
  let cache: Cache;
  let argsLength: number;
  let actualFunction;

  actualFunction = function initializeCache(): any {
    cache = new Cache(settings || defaultMemoizeSettings);
    argsLength = arguments.length;
    actualFunction = runCache;
    return actualFunction.apply(null, arguments);
  };

  function runCache(): any {
    const key = nullObject();
    let i = argsLength;
    const args = arguments;
    while (i--) { key[i] = args[i]; }
    return cache.getSync(key, () => func.apply(null, args));
  }


  const result = <any>function (): any {
    return actualFunction.apply(null, arguments);
  };
  result.reset = () => cache.clear();
  return result;
};

export function memoized<TP extends any[], TR>(func: (...p: TP) => TR, settings?: CacheSettings): MemoizedFunc<TP, TR> {
  const cache: Cache = new Cache(settings || defaultMemoizeSettings);
  const result: any = function (...args: any[]): any {
    const key = JSON.stringify(args);
    return cache.getSync(key, () => func.apply(null, args));
  };
  result.reset = () => cache.clear();
  return result;
}
