export interface CacheSettings {
  timeoutPrecision?: number;
  timeboxSize?: number;
  defaultTimeout?: number;
  disableTickOptimization?: boolean;
}

export const defaultSettings: CacheSettings = {
  timeoutPrecision: 500,
  timeboxSize: 3,
  defaultTimeout: 500,
  disableTickOptimization: false
};
