export interface ExpirationInfo {
  key: string;
  value: any;
  timeout: number;
  expiresAt: number;
}
