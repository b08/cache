import { CacheSettings, defaultSettings } from "../cacheSettings.type";
import { currentTime, currentTickTime } from "./currentTime";
import { ExpirationInfo } from "./expirationInfo.type";
import { StackTree } from "@b08/aa-tree";

const laterThan = (time: number, setTime: number) => time > setTime;
const earlierThan = (time: number, setTime: number) => time < setTime;

export type Expire = (key: any, value: any) => void;

export class Expiration {
  private stack: StackTree<ExpirationInfo, number> = new StackTree((t: ExpirationInfo) => t.expiresAt);
  private nextTime: number;
  private timeboxEnd: number;
  private timerItems: ExpirationInfo[][];
  private timer: NodeJS.Timeout;

  private currentTime: () => number;

  constructor(private expire: Expire, private settings: CacheSettings = {}) {
    this.settings = { ...defaultSettings, ...settings };
    this.currentTime = this.settings.disableTickOptimization ? currentTime : currentTickTime;
  }

  public setExpiration(key: any, value: any, timeout: number = this.settings.defaultTimeout): void {
    if (timeout === 0) { return; } // never expires
    const curTime = this.currentTime();
    const ti: ExpirationInfo = { key, value, timeout, expiresAt: curTime + timeout };
    if (this.noCurrentTimer()) { return this.startNewTimer(curTime, ti); }
    if (this.expiresAfterTimebox(ti)) { return this.stack.add(ti); }
    if (this.expiresWayBeforeTimebox(ti)) { return this.recreateTimer(curTime, ti); }
    this.addToBox(ti);
  }

  private expireItem = (item: ExpirationInfo) => this.expire(item.key, item.value);
  private expireItems = (items: ExpirationInfo[]) => items.forEach(this.expireItem);

  private noCurrentTimer = () => this.nextTime == null;
  private expiresAfterTimebox = (ti: ExpirationInfo) => laterThan(ti.expiresAt, this.timeboxEnd);
  private expiresWayBeforeTimebox = (ti: ExpirationInfo) => earlierThan(ti.expiresAt + this.settings.timeoutPrecision, this.nextTime);

  private getBoxIndex = (ti: ExpirationInfo) => {
    let index = 0;
    let nextTime = this.nextTime;
    while (ti.expiresAt > nextTime) {
      nextTime += this.settings.timeoutPrecision;
      index++;
    }
    return index;
  }

  private addToBox = (ti: ExpirationInfo) => this.timerItems[this.getBoxIndex(ti)].push(ti);

  private moveBoxItemsToTree = () => this.timerItems.slice(1).forEach(items => this.stack.addRange(items));

  private recreateTimer(curTime: number, ti: ExpirationInfo): void {
    clearTimeout(this.timer);
    this.moveBoxItemsToTree();
    this.startNewTimer(curTime, ti);
  }

  private fillTimebox = () => { for (let i = this.timerItems.length; i < this.settings.timeboxSize; i++) { this.timerItems.push([]); } };
  private startTimer = (timeout: number) => this.timer = setTimeout(() => this.timerAction(), timeout);

  private setNextTime(nextTime: number): void {
    this.nextTime = nextTime;
    this.timeboxEnd = nextTime + this.settings.timeoutPrecision * (this.settings.timeboxSize - 1);
  }

  private startNewTimer(curTime: number, ti: ExpirationInfo): void {
    const timeout = Math.max(ti.timeout, this.settings.timeoutPrecision);
    this.setNextTime(curTime + timeout);
    this.timerItems = [[ti]];
    this.fillTimebox();
    this.startTimer(timeout);
  }


  private getIndexAfterExpired = (curTime: number) => Math.ceil((curTime - this.nextTime) / this.settings.timeoutPrecision);
  private expireTimeboxUntil = (index: number) => {
    for (let i = 0; i < index && i < this.timerItems.length; i++) { this.expireItems(this.timerItems[i]); }
  }

  private expireFromStack = (curTime: number) => {
    while (!this.stack.isEmpty() && earlierThan(this.stack.first().expiresAt, curTime)) {
      this.expireItem(this.stack.shift());
    }
  }

  private boxIsEmpty = () => !this.timerItems.some(ti => ti.length > 0);

  private timerAction(): void {
    const curTime = this.currentTime();
    const indexAfterExpired = this.getIndexAfterExpired(curTime);
    this.expireTimeboxUntil(indexAfterExpired);
    this.expireFromStack(curTime);
    this.timerItems = this.timerItems.slice(indexAfterExpired);
    if (this.boxIsEmpty() && this.stack.isEmpty()) { return this.nextTime = this.timerItems = null; }
    this.setNextTime(this.nextTime + this.settings.timeoutPrecision * indexAfterExpired);
    this.fillTimebox();
    this.startTimer(this.nextTime - curTime);
  }
}
