let time: number;

export function currentTickTime(): number {
  if (time != null) {
    return time;
  }
  process.nextTick(() => time = null);
  return time = currentTime();
}

export function currentTime(): number {
  return new Date().getTime();
}
