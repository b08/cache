import { IMap } from "@b08/object-map";
export type CacheKey = IMap<any> | string | number;
