import { CacheKey } from "./cacheKey.type";
import { CacheProvider } from "./cacheProvider";
import { CacheSettings } from "./cacheSettings.type";

export class Cache {
  private provider: CacheProvider;
  constructor(private settings?: CacheSettings) {
    this.clear();
  }

  public clear(): void {
    this.provider = new CacheProvider(this.settings);
  }

  public getSync<T>(key: CacheKey, getter: (key: CacheKey) => T, timeout?: number): T {
    if (this.provider.has(key)) {
      return this.provider.get(key);
    }

    const value = getter(key);
    this.provider.set(key, value, timeout);
    return value;
  }
}
