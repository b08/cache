import { CacheProvider } from "../src";
import { describe } from "@b08/test-runner";
import { wait } from "@b08/async";
import { range, last } from "@b08/array";

describe("CacheProvider", it => {
  it("should return value if sooner than timeout", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 40 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 40);
    await wait(20);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect.equal(result, value);
    expect.true(has);
  });

  it("should return value if sooner than timeout precision", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 40 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 10);
    await wait(20);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect.equal(result, value);
    expect.true(has);
  });

  it("should return null if later than timeout", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 20 });
    const key = "1";
    const value = 1;

    // act
    target.set(key, value, 20);
    await wait(40);
    const has = target.has(key);
    const result = target.get(key);

    // assert
    expect.true(result == null);
    expect.false(has);
  });

  it("should expire items earlier", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 50 });
    const items = range(2000);

    // act
    items.forEach(i => target.set(i.toString(), i, 200));
    items.forEach(i => target.set(i.toString(), i, 20));

    await wait(50);
    // assert
    expect.false(target.has(last(items).toString()));
  });

  it("should expire items later", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 30 });
    const items = range(2000);

    // act
    items.forEach(i => target.set(i.toString(), i, 30));
    items.forEach(i => target.set(i.toString(), i, 300));


    // assert
    await wait(50);
    expect.true(target.has(last(items).toString()));
    await wait(250);
    expect.false(target.has(last(items).toString()));
  });


  it("should delete item", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 100 });
    const key = "10";
    // act
    target.set(key, 10, 0);
    target.delete(key);
    const has = target.has(key);
    const result = target.get(key);
    // assert
    expect.false(has);
    expect.true(result == null);
  });

  it("should use second bucket item", async expect => {
    // arrange
    const target = new CacheProvider({ timeoutPrecision: 25 });

    // act
    target.set("10", 10, 20);
    target.set("20", 20, 40);
    const has = target.has("20");
    await wait(70);
    const has2 = target.has("20");

    // assert
    expect.true(has);
    expect.false(has2);
  });
});
