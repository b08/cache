import { memoized } from "../src";
import { describe } from "@b08/test-runner";
import { wait } from "@b08/async";

let called = {};

function calledFunc(arg: number): number {
  called[arg] = (called[arg] || 0) + 1;
  return arg + 10;
}

describe("memoized", it => {
  it("should memoize the function result", async expect => {
    // arrange
    called = {};
    const target = memoized(calledFunc);

    // act
    target(1);
    expect.equal(called[1], 1);
    const result = target(1);

    // assert
    expect.equal(called[1], 1);
    expect.equal(result, 11);
  });

  it("should memoize 2 function results", async expect => {
    // arrange
    called = {};
    const target = memoized(calledFunc);

    // act
    target(11);
    target(11);
    target(22);
    target(22);

    // assert
    expect.equal(called[11], 1);
    expect.equal(called[22], 1);
  });

  it("should drop function result after timeout", async expect => {
    // arrange
    called = {};
    const target = memoized(calledFunc, { defaultTimeout: 30, timeoutPrecision: 30 });

    // act
    target(33);
    await wait(60);
    target(33);

    // assert
    expect.equal(called[33], 2);
  });
});
